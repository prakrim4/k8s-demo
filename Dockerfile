FROM node:10.22.0-alpine3.10 as node
WORKDIR /app
COPY package*.json ./
ARG BUILDENV
RUN npm install
COPY . .
RUN npm run start:$BUILDENV


